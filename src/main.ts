import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideRouter, RouterModule } from '@angular/router';

import { environment } from './environments/environment';

import { AppComponent } from './app/app.component';
import { APP_ROUTES } from "./app/routes";
import { CoreModule } from "./app/core/core.module";

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(
      CoreModule,
      RouterModule.forRoot(APP_ROUTES, {scrollPositionRestoration: 'top'})
    ),
  ]
})
  .catch(console.error);
