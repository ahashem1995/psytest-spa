import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'psy-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  standalone: true
})
export class FooterComponent implements OnInit {

  protected readonly year = new Date().getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

}
