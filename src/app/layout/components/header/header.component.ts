import { Component, OnInit } from '@angular/core';
import { ButtonDirective } from "@shared/directives/button/button.directive";
import { RouterLinkWithHref } from "@angular/router";

@Component({
  selector: 'psy-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  imports: [
    RouterLinkWithHref,
    ButtonDirective
  ],
  standalone: true,
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
