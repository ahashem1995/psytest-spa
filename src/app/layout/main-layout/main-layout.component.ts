import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterOutlet } from "@angular/router";
import { HeaderComponent } from "@layout/components/header/header.component";
import { FooterComponent } from "@layout/components/footer/footer.component";
import { NgClass } from "@angular/common";

@Component({
  selector: 'psy-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  imports: [
    NgClass,
    RouterOutlet,
    HeaderComponent,
    FooterComponent
  ],
  standalone: true,
})
export class MainLayoutComponent implements OnInit {

  protected smBgPosition = ['bg-bottom', 'bg-left-bottom', 'bg-right-bottom'][Math.floor((Math.random()) * 3)];
  protected mdBgPosition = ['md:bg-left-top', 'md:bg-center', 'md:bg-right' ][Math.floor((Math.random()) * 3)];

  protected isPlayfulMode = false;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isPlayfulMode = this.route.snapshot.data['bgMode'] === 'playful';
  }

}
