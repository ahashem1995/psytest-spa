import { InjectionToken, Provider } from "@angular/core";
import { environment } from "../../../environments/environment";

export const API_BASE_URL = new InjectionToken('API Base URL to set by the interceptor');

export const apiBaseUrlToken: Provider = {
  provide: API_BASE_URL,
  useValue: environment.apiUrl,
};
