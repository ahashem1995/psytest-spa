import { InjectionToken, Provider } from "@angular/core";

export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage');

export const browserStorageToken: Provider = {
  provide: BROWSER_STORAGE,
  useFactory: () => localStorage,
};
