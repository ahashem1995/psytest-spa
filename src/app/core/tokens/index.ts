import { Provider } from "@angular/core";
import { apiBaseUrlToken } from "./base-api-url.token";
import { browserStorageToken } from "./browser-storage.token";

export const injectionTokens: Provider[] = [
  apiBaseUrlToken,
  browserStorageToken
];
