import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { HttpErrorHandlerService } from "../services/http-error-handler/http-error-handler.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private handler: HttpErrorHandlerService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(catchError(error => {
        this.handler.handle(error);
        return throwError(error.message)
      }));
  }
}
