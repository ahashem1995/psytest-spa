import { TestBed } from '@angular/core/testing';

import { BaseApiUrlInterceptor } from './base-api-url.interceptor';

describe('BaseApiUrlInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      BaseApiUrlInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: BaseApiUrlInterceptor = TestBed.inject(BaseApiUrlInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
