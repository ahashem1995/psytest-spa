import { Provider } from "@angular/core";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { BaseApiUrlInterceptor } from "./base-api-url.interceptor";
import { HttpErrorInterceptor } from "./http-error.interceptor";
import { DelayInterceptor } from "./delay.interceptor";
import { environment } from "../../../environments/environment";

export const interceptors: Provider[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: BaseApiUrlInterceptor,
    multi: true,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true,
  },
  ...(environment.production
    ? []
    : [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: DelayInterceptor,
        multi: true
      }
    ])
];
