import { Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_BASE_URL } from '../tokens/base-api-url.token';

@Injectable()
export class BaseApiUrlInterceptor implements HttpInterceptor {

  constructor(@Inject(API_BASE_URL) private baseUrl: string) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const apiRequest = request.clone({ url: `${this.baseUrl}/${request.url}` });

    return next.handle(apiRequest);
  }
}
