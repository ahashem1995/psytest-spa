import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { interceptors } from "./interceptors";
import { injectionTokens } from "./tokens";
import { services } from "./services";
import { ToastrModule } from "ngx-toastr";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    ...injectionTokens,
    ...interceptors,
    ...services,
  ],
  exports: [
  ]
})
export class CoreModule {
}
