export interface ApiResponse<T extends any> {
  data: T;
}
