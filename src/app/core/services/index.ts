import { ApiService } from './api/api.service';
import { FeedbackService } from './feedback/feedback.service';
import { HttpErrorHandlerService } from './http-error-handler/http-error-handler.service';
import { LocalStorageService } from './local-storage/local-storage.service';

export const services = [
  FeedbackService,
  LocalStorageService,
  ApiService,
  HttpErrorHandlerService,
];
