import { Inject, Injectable, InjectionToken } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable, skip } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CoreModule } from "../../core.module";
import { BROWSER_STORAGE } from "../../tokens/browser-storage.token";

interface StorageChange {
  key: string;
  value: any;
}

@Injectable()
export class LocalStorageService {
  private KEY_PREFIX = 'psy-' as const;
  private keyChangeSource$ = new BehaviorSubject(this.KEY_PREFIX);
  keyChange$ = this.keyChangeSource$.asObservable();

  private changeSource$ = new BehaviorSubject<StorageChange | null>(null);
  change$ = this.changeSource$.asObservable();

  constructor(@Inject(BROWSER_STORAGE) private storage: Storage) {
  }

  itemChange$<T>(key: string, skipPrefix = false): Observable<T> {
    return fromEvent<StorageEvent>(window, 'storage')
      .pipe(
        filter(({storageArea}) => storageArea === this.storage),
        filter(({key: changeKey}) => changeKey === (skipPrefix ? '' : this.keyChangeSource$.value) + key),
        map<StorageEvent, T>(({newValue}) => {
          return JSON.parse(newValue as string);
        }),
      );
  }

  set(key: string, value: any, skipPrefix = false): void {
    const prefix = skipPrefix ? '' : this.keyChangeSource$.value;
    this.storage.setItem(prefix + key, JSON.stringify(value));
    this.changeSource$.next({key, value});
  }

  updateObjectWith<T>(key: string, newObject: Partial<T>, skipPrefix = false): T {
    const prefix = skipPrefix ? '' : this.keyChangeSource$.value;
    const prefixedKey = prefix + key;
    const previousValue = this.get<T>(prefixedKey, true);
    const newValue: T = {...previousValue, ...newObject};
    this.storage.setItem(prefixedKey, JSON.stringify(newValue));
    this.changeSource$.next({key, value: newValue});
    return newValue;
  }

  updateArrayWith<T>(key: string, newObject: T, skipPrefix = false): Array<T> {
    const prefix = skipPrefix ? '' : this.keyChangeSource$.value;
    const prefixedKey = prefix + key;
    const previousValue = this.get<Array<T>>(prefixedKey, true) ?? [];
    const newValue: Array<T> = [...previousValue, newObject];
    this.storage.setItem(prefixedKey, JSON.stringify(newValue));
    this.changeSource$.next({key, value: newValue});
    return newValue;
  }

  get<T>(key: string, skipPrefix = false): T {
    const prefix = skipPrefix ? '' : this.keyChangeSource$.value;
    const storageValue = this.storage.getItem(prefix + key);
    return JSON.parse(storageValue as string);
  }

  getAll(): Array<any> {
    const store = [];
    for (let i = 0; i < this.storage.length; i++) {
      const key = this.storage.key(i) as string;
      const value = this.get(key);
      store.push({key, value});
    }
    return store;
  }

  remove(key: string, skipPrefix = false): void {
    const prefix = skipPrefix ? '' : this.keyChangeSource$.value;
    this.storage.removeItem(prefix + key);
    this.changeSource$.next({key, value: null});
  }
}
