import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable, take } from "rxjs";
import { ApiResponse } from "../../models/api-response";

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {
  }

  getRequestRaw<T>(path: string): Observable<ApiResponse<T>> {
    return this.http.get<ApiResponse<T>>(path)
      .pipe(take(1));
  }

  getRequest<T>(path: string): Observable<T> {
    return this.getRequestRaw<T>(path)
      .pipe(map(response => response.data));
  }

  postRequest<T, E>(path: string, requestBody: E): Observable<T> {
    return this.http.post<T>(path, requestBody)
      .pipe(take(1));
  }

  putRequest<T, E>(path: string, requestBody: E): Observable<T> {
    return this.http.put<T>(path, requestBody)
      .pipe(take(1));
  }

  patchRequest<T, E>(path: string, requestBody: E): Observable<T> {
    return this.http.patch<T>(path, requestBody)
      .pipe(take(1));
  }

  deleteRequest<T>(path: string): Observable<T> {
    return this.http.delete<T>(path)
      .pipe(take(1));
  }

}
