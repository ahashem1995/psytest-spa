import { Injectable } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { FeedbackService } from "../feedback/feedback.service";

@Injectable()
export class HttpErrorHandlerService {

  constructor(private feedback: FeedbackService) { }

  public handle(response: any) {
    if (response instanceof HttpErrorResponse) {
      if (!navigator.onLine) {
        return {};
      } else if (response.status === 401) {
        return {};
      } else if (response.status === 403) {
        return {};
      } else if (response.status === 404) {
        this.feedback.error(response.error.message);
        return {};
      } else if (response.status === 422) {
        return {}
      } else if (response.status === 500) {
        this.feedback.error('Something went wrong, please try again later');
      }
      return {};
    }
    return {};
  }
}
