import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from "@angular/router";

@Component({
  selector: 'app-tests-landing',
  template: `
    <div class="tests-landing flex-1 flex flex-col">
      <router-outlet></router-outlet>
    </div>
  `,
  host: {
    'class': 'flex flex-col flex-1'
  },
  imports: [RouterOutlet],
  standalone: true
})
export class TestsLandingComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
