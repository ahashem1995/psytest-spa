import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  catchError,
  finalize,
  map,
  Observable, of,
  Subject,
  switchMap,
  take,
  tap,
  throwError,
  zip
} from 'rxjs';
import { TestService } from '../services/test.service';
import { Test, TestRef } from '../interfaces/test';
import { QuestionAnswers } from "../interfaces/question-answer";
import { TestResult } from "../interfaces/test-result";
import { TestHistory } from "../interfaces/test-history";

@Injectable()
export class TestState {

  // Test details
  private readonly _activeIdx$ = new BehaviorSubject<number>(0);
  private readonly _test$ = new BehaviorSubject<Test | null>(null);
  private readonly _isLoading$ = new BehaviorSubject<boolean>(false);
  private readonly _isSubmitting$ = new BehaviorSubject<boolean>(false);

  public activeIdx$ = this._activeIdx$.asObservable();
  public test$ = this._test$.asObservable();
  public isLoading$ = this._isLoading$.asObservable();
  public isSubmitting$ = this._isSubmitting$.asObservable();

  // Test list
  private readonly _tests$ = new BehaviorSubject<Array<TestRef>>([]);
  private readonly _testsIsLoading$ = new BehaviorSubject<boolean>(false);

  public tests$ = this._tests$.asObservable();
  public testsIsLoading$ = this._testsIsLoading$.asObservable();

  // Test result
  private readonly _result$ = new BehaviorSubject<TestResult | null>(null);
  private readonly _resultIsLoading$ = new BehaviorSubject<boolean>(false);

  public result$ = this._result$.asObservable();
  public resultIsLoading$ = this._resultIsLoading$.asObservable();

  // Stepper
  private readonly _submitSucceed$ = new Subject<TestHistory>();
  public submitSucceed$ = this._submitSucceed$.asObservable();

  constructor(private svc: TestService) {
  }

  public setActiveIdx(idx: number): void {
    this._activeIdx$.next(idx);
  }

  public getTestDetails$(slug$: Observable<string>) {
    return slug$.pipe(
      tap(() => this._isLoading$.next(true)),
      switchMap(slug => this.svc.getTestDetails$(slug)),
      take(1),
      tap(test => this._test$.next(test)),
      catchError((error) => throwError(error)),
      finalize(() => this._isLoading$.next(false))
    )
  }

  public getTestList(): void {
    this._testsIsLoading$.next(true);

    this.svc.getTestList$()
      .pipe(
        catchError(() => of([])),
        finalize(() => this._testsIsLoading$.next(false))
      )
      .subscribe(tests => this._tests$.next(tests));
  }

  public submitTestAnswers$(id$: Observable<number>, answers: QuestionAnswers): Observable<string> {
    const questionIds = Object.keys(answers).map(Number);
    const payload = questionIds
      .reduce((acc: Array<{ questionId: number, optionId: number }>, curr) =>
        [...acc, { questionId: curr, optionId: answers[curr]!.id }], []);

    return id$.pipe(
      tap(() => this._isSubmitting$.next(true)),
      switchMap(id => this.svc.submitTestAnswers$(id, { answers: payload })),
      tap(() => this._activeIdx$.next(0)),
      map(({ data: { token } }) => token),
      finalize(() => this._isSubmitting$.next(false)),
    );
  }

  public getTestResult$(slug$: Observable<string>, token$: Observable<string>): Observable<TestResult> {
    return zip([slug$, token$]).pipe(
      tap(() => this._resultIsLoading$.next(true)),
      switchMap(([slug, token]) => this.svc.getTestResult$(slug, token)),
      map(result => new TestResult(result.test, result.answers, result.text, result.heading)),
      tap(testResult => this._result$.next(testResult)),
      finalize(() => this._resultIsLoading$.next(false)),
    );
  }

  public emitTestSubmission(payload: TestHistory): void {
    this._submitSucceed$.next(payload);
  }


}
