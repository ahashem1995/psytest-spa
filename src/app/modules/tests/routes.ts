import { Route, Routes } from "@angular/router";
import { MainLayoutComponent } from "@layout/main-layout/main-layout.component";
import { TestService } from "./services/test.service";
import { TestState } from "./states/test.state";
import { TestHistoryService } from "./services/test-history.service";
import { TestFacade } from "./facades/test.facade";

const wrapWithLandingParent: (children: Routes, bgMode: 'playful' | 'elegant') => Route =
  (children, bgMode) => ({
    path: '',
    component: MainLayoutComponent,
    data: { bgMode },
    providers: [
      TestService,
      TestState,
      TestHistoryService,
      TestFacade,
    ],
    children: [
      {
        path: '',
        loadComponent: () => import('./tests-landing.component').then(m => m.TestsLandingComponent),
        children,
      }
    ]
  });

export const TEST_ROUTES: Route[] = [
  wrapWithLandingParent([
    { path: '', loadComponent: () => import('./pages/tests/tests.page').then(mod => mod.TestsPage) },
  ], 'playful'),

  wrapWithLandingParent([
    {
      path: 'tests',
      children: [
        {
          path: 'history',
          loadComponent: () => import('./pages/tests-history/tests-history.page').then(m => m.TestsHistoryPage),
        },
        {
          path: ':slug',
          children: [
            {
              path: 'result/:token',
              loadComponent: () => import('./pages/test-result/test-result.page').then(m => m.TestResultPage)
            },
            {
              path: '',
              loadComponent: () => import('./pages/test-details/test-details.page').then(m => m.TestDetailsPage)
            }
          ]
        }
      ]
    }
  ], 'elegant'),
];
