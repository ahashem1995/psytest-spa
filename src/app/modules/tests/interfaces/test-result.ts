import { Test } from "./test";
import { QuestionAnswer, QuestionAnswers } from "./question-answer";

export class TestResult {
  private readonly _test: Test;
  private readonly _answers: Array<QuestionAnswer>;
  private readonly _text: string;
  private readonly _heading: string;
  private readonly _normalizedAnswers: QuestionAnswers;

  constructor(test: Test, answers: Array<QuestionAnswer>, text: string, heading: string) {
    this._test = test;
    this._answers = answers;
    this._text = text;
    this._heading = heading;
    this._normalizedAnswers = this.normalizeAnswers();
  }

  private normalizeAnswers() {
    const question = ({ questionId }: QuestionAnswer) =>
      this._test.questions.find(q => q.id === questionId)!;
    const option = (answer: QuestionAnswer) =>
      question(answer).options.find(o => o.id === answer.optionId);

    return this._answers.reduce((acc, curr) =>
      ({ ...acc, [curr.questionId]: option(curr) }), {});
  }

  get test() {
    return this._test;
  }

  get answers() {
    return this._answers;
  }

  get normalizedAnswers() {
    return this._normalizedAnswers;
  }

  get text() {
    return this._text;
  }

  get heading() {
    return this._heading;
  }
}
