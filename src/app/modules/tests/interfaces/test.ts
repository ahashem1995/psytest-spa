import { Question } from "./question";

export interface TestRef {
  id: number
  slug?: string;
  title: string;
  image?: string;
  disabled?: boolean;
}

export interface Test extends TestRef {
  questions: Array<Question>;
}
