import { QuestionOption } from "./question-option";

export interface Question {
  id: number;
  title: string;
  options: Array<QuestionOption>;
}
