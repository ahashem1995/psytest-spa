import { TestRef } from "./test";

export interface TestHistory extends TestRef {
  token: string;
  createdAt: Date;
}
