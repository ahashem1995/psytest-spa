export interface TestSubmission {
  answers: Array<{ questionId: number; optionId: number }>;
}
