import { QuestionOption } from "./question-option";

export type NullableAnswer = QuestionOption | null;

export type QuestionAnswers = {[key: number]: NullableAnswer};

export interface QuestionAnswer {
  questionId: number;
  optionId: number;
}
