import { Component, OnInit } from '@angular/core';
import { TestHistory } from '../../interfaces/test-history';
import { TestFacade } from '../../facades/test.facade';
import { CommonModule } from '@angular/common';
import { RouterLinkWithHref } from '@angular/router';
import { TestItemComponent } from '../../components/test-item/test-item.component';
import { PageLayoutComponent } from '../../components/page-layout/page-layout.component';
import { TrackByPropertyDirective } from "@shared/directives/track-by-property/track-by-property.directive";
import { ButtonDirective } from "@shared/directives/button/button.directive";

type HistoryGroup = TestHistory & { active: boolean, items: Array<{ date: Date, token: string }> }

@Component({
  selector: 'psy-tests-history',
  templateUrl: './tests-history.page.html',
  styleUrls: ['./tests-history.page.scss'],
  imports: [
    CommonModule,
    RouterLinkWithHref,
    ButtonDirective,
    TrackByPropertyDirective,
    PageLayoutComponent,
    TestItemComponent
  ],
  host: {
    'class': 'flex flex-col flex-1'
  },
  standalone: true,
})
export class TestsHistoryPage implements OnInit {

  protected history: Array<HistoryGroup> = this.tests
    .getHistory()
    .reduce((acc: Array<HistoryGroup>, curr: TestHistory) => {
      const prev = acc.find(historyItem => historyItem.id === curr.id);
      if (prev) {
        prev.items.unshift({ token: curr.token, date: curr.createdAt });
      } else {
        acc = [{ ...curr, active: false, items: [{ date: curr.createdAt, token: curr.token }] }, ...acc];
      }
      return acc;
    }, []);

  constructor(private tests: TestFacade) {
  }

  ngOnInit(): void {
  }

}
