import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestsHistoryPage } from './tests-history.page';

describe('TestsHistoryComponent', () => {
  let component: TestsHistoryPage;
  let fixture: ComponentFixture<TestsHistoryPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestsHistoryPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestsHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
