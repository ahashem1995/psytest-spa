import { Component, OnInit } from '@angular/core';
import { TestFacade } from "../../facades/test.facade";
import { ActivatedRoute, RouterLinkWithHref } from "@angular/router";
import { map, take } from "rxjs";
import { Test } from "../../interfaces/test";
import { PageLayoutComponent } from "../../components/page-layout/page-layout.component";
import { TestTemplateComponent } from "../../components/test-template/test-template.component";
import { LoaderComponent } from "@shared/components/loader/loader.component";
import { ButtonDirective } from "@shared/directives/button/button.directive";
import { TestSuggestionsComponent } from "../../components/test-suggestions/test-suggestions.component";
import { TestListComponent } from "../../components/test-list/test-list.component";
import {
  TestQuestionOptionsPickerComponent
} from "../../components/test-question-options-picker/test-question-options-picker.component";
import { CommonModule } from "@angular/common";

@Component({
  templateUrl: './test-result.page.html',
  styleUrls: ['./test-result.page.scss'],
  host: {
    'class': 'flex flex-col flex-1'
  },
  imports: [
    CommonModule,
    RouterLinkWithHref,
    ButtonDirective,
    PageLayoutComponent,
    LoaderComponent,
    TestTemplateComponent,
    TestSuggestionsComponent,
    TestListComponent,
    TestQuestionOptionsPickerComponent
  ],
  standalone: true
})
export class TestResultPage implements OnInit {

  protected showAnswers = false;

  protected result$ = this.tests.result$;
  protected isLoading$ = this.tests.resultIsLoading$;

  private slug$ = this.route.paramMap
    .pipe(map(paramMap => paramMap.get('slug') as string));

  private token$ = this.route.paramMap
    .pipe(map(paramMap => paramMap.get('token') as string));

  constructor(private route: ActivatedRoute,
              private tests: TestFacade) {
  }

  ngOnInit(): void {
    this.tests.getTestResult$(this.slug$, this.token$)
      .pipe(take(1))
      .subscribe();
  }

  isOwner(test: Test) {
    return this.tests.isOwner(test, this.route.snapshot.paramMap.get('token') as string);
  }

  toggleAnswers(footerContainer: HTMLDivElement) {
    this.showAnswers = !this.showAnswers;
    if (this.showAnswers) {
      const timeoutId = setTimeout(() => {
        footerContainer.scrollIntoView({ behavior: 'smooth' });
        clearTimeout(timeoutId);
      });
    }
  }
}
