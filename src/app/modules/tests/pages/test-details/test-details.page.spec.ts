import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDetailsPage } from './test-details.page';

describe('TestDetailsPage', () => {
  let component: TestDetailsPage;
  let fixture: ComponentFixture<TestDetailsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestDetailsPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
