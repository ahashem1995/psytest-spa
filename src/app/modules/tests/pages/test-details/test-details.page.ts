import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, Subject, take, timer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TestFacade } from '../../facades/test.facade';
import { FeedbackService } from "../../../../core/services/feedback/feedback.service";
import { Test } from "../../interfaces/test";
import { OverlayComponent } from "@shared/components/overlay/overlay.component";
import { TestSuggestionsComponent } from "../../components/test-suggestions/test-suggestions.component";
import { CommonModule } from "@angular/common";
import { TestTemplateComponent } from "../../components/test-template/test-template.component";
import { TestStepperComponent } from "../../components/test-stepper/test-stepper.component";
import { LoaderComponent } from "@shared/components/loader/loader.component";
import { TestStepperProgressComponent } from "../../components/test-stepper-progress/test-stepper-progress.component";
import { PageLayoutComponent } from "../../components/page-layout/page-layout.component";
import { ButtonDirective } from "@shared/directives/button/button.directive";

@Component({
  templateUrl: './test-details.page.html',
  styleUrls: ['./test-details.page.scss'],
  host: {
    'class': 'flex flex-col flex-1'
  },
  imports: [
    CommonModule,
    ButtonDirective,
    OverlayComponent,
    LoaderComponent,
    PageLayoutComponent,
    TestTemplateComponent,
    TestStepperComponent,
    TestStepperProgressComponent,
    TestSuggestionsComponent
  ],
  standalone: true,
})
export class TestDetailsPage implements OnInit, OnDestroy {

  // State slices
  protected activeIdx$ = this.test.activeIdx$;
  protected test$ = this.test.test$;
  protected isLoading$ = this.test.isLoading$;
  protected isSubmitting$ = this.test.isSubmitting$;

  // Action notifiers
  protected prev$ = new Subject<void>();
  protected next$ = new Subject<void>();
  protected submit$ = new Subject<void>();

  protected slug$ = this.route.paramMap
    .pipe(map(paramMap => paramMap.get('slug') as string));

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private test: TestFacade,
    private feedback: FeedbackService
  ) {
  }

  ngOnInit(): void {
    this.test.getTestDetails$(this.slug$)
      .subscribe({
        error: () => {
          this.feedback.info(`You'll be redirected within 5 seconds`);
          timer(5000)
            .pipe(take(1))
            .subscribe(() => this.router.navigate(['/']).then())
        }
      })
  }

  ngOnDestroy(): void {
    this.test.setActiveIdx(0);
  }

  setActiveIdx(idx: number): void {
    this.test.setActiveIdx(idx);
  }

  onSuccess(token: string, test: Test) {
    this.test.emitTestSubmission({
      id: test.id,
      title: test.title,
      slug: test.slug,
      image: test.image,
      token,
      createdAt: new Date()
    });
    this.router
      .navigate(['/', 'tests', this.route.snapshot.paramMap.get('slug'), 'result', token])
      .then();
  }

  onFailure(error: any) {
    // TODO:
  }
}
