import { Component, OnInit } from '@angular/core';
import { TestListComponent } from "../../components/test-list/test-list.component";
import { TestsHeroSectionComponent } from "../../components/tests-hero-section/tests-hero-section.component";
import { PageLayoutComponent } from "../../components/page-layout/page-layout.component";

@Component({
  templateUrl: './tests.page.html',
  styleUrls: ['./tests.page.scss'],
  imports: [
    PageLayoutComponent,
    TestsHeroSectionComponent,
    TestListComponent
  ],
  standalone: true
})
export class TestsPage implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
