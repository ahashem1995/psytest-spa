import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api/api.service';
import { Observable } from 'rxjs';
import { Test, TestRef } from '../interfaces/test';
import { ApiResponse } from "../../../core/models/api-response";
import { TestResult } from "../interfaces/test-result";
import { TestSubmission } from "../interfaces/test-submission";

@Injectable()
export class TestService {

  constructor(private readonly api: ApiService) { }

  public getTestDetails$(slug: string): Observable<Test> {
    return this.api.getRequest<Test>(`tests/${slug}`);
  }

  public getTestList$(): Observable<Array<TestRef>> {
    return this.api.getRequest<Array<TestRef>>(`tests`);
  }

  public submitTestAnswers$(id: number, payload: TestSubmission) {
    return this.api.postRequest<ApiResponse<{token: string}>, TestSubmission>(`tests/${id}/answers`, payload);
  }

  public getTestResult$(slug: string, token: string) {
    return this.api.getRequest<TestResult>(`tests/${slug}/results/${token}`);
  }
}
