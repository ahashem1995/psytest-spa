import { Injectable, OnDestroy } from '@angular/core';
import { LocalStorageService } from "../../../core/services/local-storage/local-storage.service";
import { Observable, Subject, takeUntil } from "rxjs";
import { TestHistory } from "../interfaces/test-history";
import { Test } from "../interfaces/test";

@Injectable()
export class TestHistoryService implements OnDestroy {

  private key = 'test-history' as const;

  unsubscribe$ = new Subject<void>();

  constructor(private storage: LocalStorageService) {
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public listen(submitSucceed$: Observable<TestHistory>): void {
    submitSucceed$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((history: TestHistory) => {
        this.storage.updateArrayWith(this.key, history);
      });
  }

  public getAll(): Array<TestHistory> {
    return this.storage.get<Array<TestHistory>>(this.key) ?? [];
  }

  public isOwner(test: Test, token: string): boolean {
    const history = this.getAll();
    return history.some(h => h.slug === test.slug && h.token === token);
  }
}
