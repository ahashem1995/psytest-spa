import { Injectable } from '@angular/core';
import { TestState } from "../states/test.state";
import { TestHistoryService } from "../services/test-history.service";

@Injectable()
export class TestFacade {
  // Test details
  public activeIdx$ = this.state.activeIdx$;
  public test$ = this.state.test$;
  public isLoading$ = this.state.isLoading$;
  public isSubmitting$ = this.state.isSubmitting$;

  public setActiveIdx = this.state.setActiveIdx.bind(this.state);
  public getTestDetails$ = this.state.getTestDetails$.bind(this.state);

  // Stepper
  public submitTestAnswers$ = this.state.submitTestAnswers$.bind(this.state);
  public emitTestSubmission = this.state.emitTestSubmission.bind(this.state);

  // Tests overview
  public tests$ = this.state.tests$;
  public testsIsLoading$ = this.state.testsIsLoading$;

  // Test result
  public result$ = this.state.result$;
  public resultIsLoading$ = this.state.resultIsLoading$;
  public getTestResult$ = this.state.getTestResult$.bind(this.state);

  public getTestList = this.state.getTestList.bind(this.state);

  // Test history
  public getHistory = this.history.getAll.bind(this.history);
  public isOwner = this.history.isOwner.bind(this.history);

  constructor(private readonly state: TestState,
              private readonly history: TestHistoryService) {
    this.history.listen(this.state.submitSucceed$);
  }
}
