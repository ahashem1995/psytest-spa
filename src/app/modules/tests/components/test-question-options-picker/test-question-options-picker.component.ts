import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionOption } from '../../interfaces/question-option';
import { NullableAnswer } from "../../interfaces/question-answer";
import { TestOptionItemComponent } from "../test-option-item/test-option-item.component";
import { UnicodeToCharacterPipe } from "@shared/pipes/unicode-to-character.pipe";
import { NgForOf } from "@angular/common";
import { TrackByPropertyDirective } from "@shared/directives/track-by-property/track-by-property.directive";

@Component({
  selector: 'psy-test-question-options-picker',
  templateUrl: './test-question-options-picker.component.html',
  styleUrls: ['./test-question-options-picker.component.scss'],
  imports: [
    NgForOf,
    TrackByPropertyDirective,
    TestOptionItemComponent,
    UnicodeToCharacterPipe
  ],
  standalone: true,
})
export class TestQuestionOptionsPickerComponent implements OnInit {

  @Input() options!: Array<QuestionOption>;

  private _selectedOption!: NullableAnswer;
  @Input() set selectedOption(value: NullableAnswer) {
    this._selectedOption = value;
    if (value !== null) {
      this.selectedOptionChange.emit(value);
    }
  }
  get selectedOption() {
    return this._selectedOption;
  }

  @Input() disabled = false;

  @Output() selectedOptionChange = new EventEmitter<QuestionOption>();

  constructor() { }

  ngOnInit(): void {
  }

}
