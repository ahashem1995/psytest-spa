import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestQuestionOptionsPickerComponent } from './test-question-options-picker.component';

describe('TestQuestionOptionsPickerComponent', () => {
  let component: TestQuestionOptionsPickerComponent;
  let fixture: ComponentFixture<TestQuestionOptionsPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestQuestionOptionsPickerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestQuestionOptionsPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
