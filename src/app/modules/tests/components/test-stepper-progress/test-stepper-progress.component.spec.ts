import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestStepperProgressComponent } from './test-stepper-progress.component';

describe('TestStepperProgressComponent', () => {
  let component: TestStepperProgressComponent;
  let fixture: ComponentFixture<TestStepperProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestStepperProgressComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestStepperProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
