import { Component, Input, OnInit } from '@angular/core';
import { NgStyle } from "@angular/common";

@Component({
  selector: 'psy-test-stepper-progress',
  templateUrl: './test-stepper-progress.component.html',
  styleUrls: ['./test-stepper-progress.component.scss'],
  host: {
    class: 'absolute bottom-0 left-0 w-full'
  },
  imports: [NgStyle],
  standalone: true,
})
export class TestStepperProgressComponent implements OnInit {

  @Input() current!: number;

  @Input() total!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
