import { Component, Input, OnInit } from '@angular/core';
import { TestRef } from '../../interfaces/test';
import { NgClass } from "@angular/common";

@Component({
  selector: 'psy-test-item',
  templateUrl: './test-item.component.html',
  styleUrls: ['./test-item.component.scss'],
  imports: [NgClass],
  standalone: true
})
export class TestItemComponent implements OnInit {

  @Input() testItem!: TestRef;

  constructor() { }

  ngOnInit(): void {
  }

}
