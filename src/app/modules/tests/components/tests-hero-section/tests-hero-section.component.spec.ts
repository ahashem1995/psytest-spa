import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestsHeroSectionComponent } from './tests-hero-section.component';

describe('TessHeroSectionComponent', () => {
  let component: TestsHeroSectionComponent;
  let fixture: ComponentFixture<TestsHeroSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestsHeroSectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestsHeroSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
