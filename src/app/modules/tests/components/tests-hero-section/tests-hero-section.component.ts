import { Component, OnInit } from '@angular/core';
import { ButtonDirective } from "@shared/directives/button/button.directive";
import { RouterLinkWithHref } from "@angular/router";

@Component({
  selector: 'psy-tests-hero-section',
  templateUrl: './tests-hero-section.component.html',
  styleUrls: ['./tests-hero-section.component.scss'],
  imports: [RouterLinkWithHref, ButtonDirective],
  standalone: true
})
export class TestsHeroSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
