import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import {
  BehaviorSubject,
  catchError,
  filter,
  map,
  of,
  Subject,
  switchMap,
  take,
  takeUntil,
  tap,
  throwError,
  timer
} from 'rxjs';
import { Question } from '../../interfaces/question';
import { QuestionOption } from '../../interfaces/question-option';
import { QuestionAnswers } from '../../interfaces/question-answer';
import { TestFacade } from '../../facades/test.facade';
import { Test } from '../../interfaces/test';
import { fadeIn } from "@shared/animations/fade-in.animation";
import { AsyncPipe, NgClass, NgIf } from "@angular/common";
import {
  TestQuestionOptionsPickerComponent
} from "../test-question-options-picker/test-question-options-picker.component";
import { NgIfAnimationDirective } from "@shared/directives/ng-if-animation/ng-if-animation.directive";

@Component({
  selector: 'psy-test-stepper',
  templateUrl: './test-stepper.component.html',
  styleUrls: ['./test-stepper.component.scss'],
  imports: [
    NgClass,
    NgIf,
    AsyncPipe,
    TestQuestionOptionsPickerComponent,
    NgIfAnimationDirective
  ],
  standalone: true,
  animations: [
    fadeIn
  ],
})
export class TestStepperComponent implements OnInit, OnDestroy {

  // Data objects
  @Input() test!: Test;
  @Output() activeIdxChange = new EventEmitter<number>();

  private _activeIdx = 0;
  @Input() set activeIdx(value: number) {
    this._activeIdx = value;
    this.activeIdxChange.emit(value);
    this.showErrorMessage$.next(false);
  }

  get activeIdx() {
    return this._activeIdx;
  }

  // Action notifiers
  @Input() next$ = new Subject<void>();
  @Input() prev$ = new Subject<void>();
  @Input() submit$ = new Subject<void>();

  // Result
  @Output() success = new EventEmitter<string>();
  @Output() failure = new EventEmitter<any>();

  protected answers: QuestionAnswers = {};

  protected showErrorMessage$ = new BehaviorSubject<boolean>(false);

  private unsubscribe$ = new Subject<void>();

  constructor(private tests: TestFacade) {
  }

  ngOnInit(): void {
    this.next$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.goNext());

    this.prev$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.goPrev());

    this.handleSubmission();

    this.test.questions.forEach(q => this.answers[q.id] = null);
  }

  private handleSubmission() {
    this.submit$
      .pipe(
        map(() => this.answers[this.activeQuestion!.id] !== null),
        tap(hasSelectedAnswer => !hasSelectedAnswer && this.showErrorMessage()),
        filter(hasSelectedAnswer => hasSelectedAnswer),
        switchMap(() => this.tests.submitTestAnswers$(of(this.test.id), this.answers)
          .pipe(catchError((error) => throwError(error)))),
        takeUntil(this.unsubscribe$)
      )
      .subscribe({
        next: token => this.success.emit(token),
        error: error => this.failure.emit(error),
      });
  }

  setAnswer($event: QuestionOption) {
    this.answers[this.activeQuestion!.id] = $event;
  }

  goNext(): void {
    const isLastStep = this.activeIdx === this.test.questions.length - 1;
    if (isLastStep) {
      return;
    }

    const hasSelectedAnswer = this.answers[this.activeQuestion!.id] !== null;
    if (!hasSelectedAnswer) {
      this.showErrorMessage();
      return;
    }

    this.activeIdx++;
  }

  goPrev(): void {
    const isFirstStep = this.activeIdx === 0;
    if (isFirstStep) {
      return;
    }

    this.activeIdx--;
  }

  private showErrorMessage(): void {
    this.showErrorMessage$.next(true);
    this.hideErrorMessageAfter(3);
  }

  private hideErrorMessageAfter(seconds: number) {
    timer(seconds * 1000)
      .pipe(take(1))
      .subscribe(() => this.showErrorMessage$.next(false));
  }

  get activeQuestion(): Question | null {
    return this.test.questions.length > 0 ? this.test.questions[this.activeIdx] : null;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
