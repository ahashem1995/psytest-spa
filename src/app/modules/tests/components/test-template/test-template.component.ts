import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'psy-test-template',
  templateUrl: './test-template.component.html',
  styleUrls: ['./test-template.component.scss'],
  standalone: true,
})
export class TestTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
