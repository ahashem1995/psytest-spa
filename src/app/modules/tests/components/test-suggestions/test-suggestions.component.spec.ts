import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSuggestionsComponent } from './test-suggestions.component';

describe('TestSuggestionsComponent', () => {
  let component: TestSuggestionsComponent;
  let fixture: ComponentFixture<TestSuggestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestSuggestionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestSuggestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
