import { Component, OnInit } from '@angular/core';
import { map } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { TestListComponent } from "../test-list/test-list.component";
import { AsyncPipe } from "@angular/common";

@Component({
  selector: 'psy-test-suggestions',
  templateUrl: './test-suggestions.component.html',
  styleUrls: ['./test-suggestions.component.scss'],
  imports: [AsyncPipe, TestListComponent],
  standalone: true,
})
export class TestSuggestionsComponent implements OnInit {

  slug$ = this.route.paramMap.pipe(map(paramMap => paramMap.get('slug')));

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

}
