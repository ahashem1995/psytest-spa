import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestOptionItemComponent } from './test-option-item.component';

describe('TestOptionItemComponent', () => {
  let component: TestOptionItemComponent;
  let fixture: ComponentFixture<TestOptionItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestOptionItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestOptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
