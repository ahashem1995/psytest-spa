import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionOption } from "../../interfaces/question-option";
import { NgClass } from "@angular/common";

@Component({
  selector: 'psy-test-option-item',
  templateUrl: './test-option-item.component.html',
  styleUrls: ['./test-option-item.component.scss'],
  imports: [NgClass],
  standalone: true
})
export class TestOptionItemComponent implements OnInit {

  @Input() option!: QuestionOption;

  @Input() isSelected!: boolean;

  @Input() orderLetter!: string;

  @Input() disabled = false;

  @Output() optionChange = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

}
