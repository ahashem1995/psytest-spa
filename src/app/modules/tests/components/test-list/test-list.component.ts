import { Component, Input, OnInit } from '@angular/core';
import { TestRef } from "../../interfaces/test";
import { TestFacade } from "../../facades/test.facade";
import { map } from "rxjs";
import { LoaderComponent } from "@shared/components/loader/loader.component";
import { CommonModule } from "@angular/common";
import { TestItemComponent } from "../test-item/test-item.component";
import { RouterLinkWithHref } from "@angular/router";
import { TrackByPropertyDirective } from "@shared/directives/track-by-property/track-by-property.directive";

@Component({
  selector: 'psy-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss'],
  imports: [
    RouterLinkWithHref,
    CommonModule,
    TrackByPropertyDirective,
    LoaderComponent,
    TestItemComponent
  ],
  standalone: true
})
export class TestListComponent implements OnInit {

  @Input() excludeSlug: string | null = '';

  // State slices
  protected tests$ = this.tests.tests$
    .pipe(
      map(tests => tests.filter(t => t.slug !== this.excludeSlug))
    );
  protected isLoading$ = this.tests.testsIsLoading$;

  constructor(private tests: TestFacade) {
  }

  ngOnInit(): void {
    this.tests.getTestList();
  }

  trackTestById(_: any, testItem: TestRef) {
    return testItem.id;
  }

}
