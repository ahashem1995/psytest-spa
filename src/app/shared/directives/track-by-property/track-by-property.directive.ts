import { Directive, Host, Input } from '@angular/core';
import { NgForOf } from "@angular/common";

@Directive({
  selector: '[ngForTrackByProperty]',
  standalone: true
})
export class TrackByPropertyDirective {

  private property = "";

  @Input("ngForTrackByProperty")
  set propertyName(value: string | null) {
    this.property = value ?? "";
  }

  constructor(@Host() private readonly ngFor: NgForOf<any>) {
    this.ngFor.ngForTrackBy = (_: number, item: any) => this.property ? item[this.property] : item;
  }

}
