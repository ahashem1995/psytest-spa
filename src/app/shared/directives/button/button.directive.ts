import { Directive, ElementRef, Input, OnInit } from '@angular/core';

const BASE_CLASSES = 'px-4 py-2 font-semibold rounded-lg shadow-sm';
const DISABLED_CLASSES = `opacity-75 cursor-not-allowed`;
const PRIMARY_CLASSES = `${BASE_CLASSES} bg-[#e19435] text-white`;
const PRIMARY_HOVER_CLASSES = `hover:bg-[#d97101]`;

const SECONDARY_CLASSES = `${BASE_CLASSES} bg-slate-100 text-black`;
const SECONDARY_HOVER_CLASSES = `hover:bg-slate-200`;

type ButtonColor = 'primary' | 'secondary';

@Directive({
  selector: '[psyButton]',
  standalone: true
})
export class ButtonDirective implements OnInit {

  private initialClasses: string = '';

  @Input() color: ButtonColor = 'primary';

  private _disabled: boolean = false;
  @Input() set disabled(value: boolean | null) {
    this._disabled = !!value;
    this.elementRef.nativeElement.disabled = this._disabled;
    this.updateClassList();
  }

  private colorMap: { [key in ButtonColor]: string } = {
    primary: PRIMARY_CLASSES,
    secondary: SECONDARY_CLASSES,
  };

  private colorHoverMap: { [key in ButtonColor]: string } = {
    primary: PRIMARY_HOVER_CLASSES,
    secondary: SECONDARY_HOVER_CLASSES,
  };

  constructor(private elementRef: ElementRef<HTMLButtonElement>) {
    this.storeInitialClasses();
  }

  ngOnInit() {
    this.updateClassList();
  }

  private updateClassList(): void {
    let classes = this.colorMap[this.color];

    if (this._disabled) {
      classes += ` ${DISABLED_CLASSES}`;
    } else {
      classes += ` ${this.colorHoverMap[this.color]}`;
    }


    this.elementRef.nativeElement.className = `${classes} ${this.initialClasses}`;
  }

  private storeInitialClasses() {
    this.initialClasses = this.elementRef.nativeElement.className;
  }
}
