import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unicodeToCharacter',
  standalone: true,
})
export class UnicodeToCharacterPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    return String.fromCharCode(value);
  }

}
