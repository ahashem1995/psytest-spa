import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'psy-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss'],
  standalone: true
})
export class OverlayComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
